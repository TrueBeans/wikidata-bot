declare module "wikibase-edit" {
  /**
   * Configuration options for a wikibase-edit instance.
   */
  interface WikibaseConfig {
    /**
     * A Wikibase instance URL.
     */
    instance: string

    /**
     * The instance script path, used to find the API endpoint.
     * Default: /w.
     */
    wgScriptPath?: string

    /**
     * One authorization method is required (unless in anonymous mode).
     */
    credentials?: {
      /**
       * A Wikibase username.
       */
      username?: string

      /**
       * A Wikibase password.
       */
      password?: string

      /**
       * OAuth tokens. Can be used instead of username/password authentication.
       */
      oauth?: {
        /**
         * Obtained at registration. See https://www.mediawiki.org/wiki/OAuth/For_Developers#Registration.
         */
        consumer_key: string

        /**
         * Obtained at registration. See https://www.mediawiki.org/wiki/OAuth/For_Developers#Registration.
         */
        consumer_secret: string

        /**
         * Obtained when the user authorized your service. See https://www.mediawiki.org/wiki/OAuth/For_Developers#Authorization.
         */
        token: string

        /**
         * Obtained when the user authorized your service. See https://www.mediawiki.org/wiki/OAuth/For_Developers#Authorization.
         */
        token_secret: string
      }
    }

    /**
     * Flag to activate the 'anonymous' mode, which actually isn't anonymous as it signs with your IP.
     * Default: false.
     */
    anonymous?: boolean

    /**
     * An edit summary that will be common to all the edits.
     * See https://meta.wikimedia.org/wiki/Help:Edit_summary.
     * Default: empty.
     */
    summary?: string

    /**
     * An array of tags for the edits.
     * See https://www.mediawiki.org/wiki/Manual:Tags.
     * Default: on Wikidata [ 'WikibaseJS-edit' ], empty for other Wikibase instances
     */
    tags?: string[]

    /**
     * A custom user-agent header.
     * Default: `wikidata-edit/${pkg.version} (https://github.com/maxlath/wikidata-edit)`.
     */
    userAgent?: string

    /**
     * See https://www.mediawiki.org/wiki/Manual:Bots.
     * Default: false.
     */
    bot?: boolean

    /**
     * See https://www.mediawiki.org/wiki/Manual:Maxlag_parameter.
     * Default: 5.
     */
    maxlag?: number
  }

  type WikibaseEdit = (generalConfig: WikibaseConfig) => any
  const wikibaseEdit: WikibaseEdit
  export default wikibaseEdit
}
