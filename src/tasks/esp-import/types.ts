export type RawProduct = typeof import("./products.json")[number]

export interface ProductSpecs {
  /** Numerical identifier used by the products.espressif.com API */
  id: number
  /**
   * The name of the product, which includes its family.
   * Name of the product series, e.g. ESP32-WROVER-E or ESP32-S3
   * Named as "part number" in the product selector's filter settings
   */
  familyName: string
  /**
   * The type of product.
   * `SYSTEM_ON_CHIP` (SoC) is a small chip containing the microcontroller
   * `MODULE` is the SoC mounted on a PCB, and includes some additional components such as an antenna
   * @link https://www.reddit.com/r/esp32/comments/lddrmt/comment/gm5iexh
   */
  type: ProductType
  /** Width, height and depth of the device @unit mm */
  dimensions: Dimensions
  /** For SoCs only. The name of the socket the chip is designed to fit into. */
  socketName?: string
  /** Wi-Fi standard compatibility, if any, including Wi-Fi 4 and Wi-Fi 6 */
  wifi: WiFiAbility[]
  /** Supported Bluetooth (BT) technology types, including BT Classic and BLE */
  bluetooth: BluetoothAbility[]
  /** Total number of pins present, including GPIO */
  pins: number
  /** CPU (maximum?) clock frequency, in MHz */
  clockFrequency: number
  /**
   * The capacity of the device's SRAM (Static Random Access Memory)
   * @link https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-guides/memory-types.html
   * @unit KB
   */
  sramCapacity: number
  /** The capacity of the device's Read-Only Memory @unit KB */
  romCapacity: number
  /**
   * Capacity of non-volatile flash memory available on the device.
   * Can be `0`, meaning that this SoC has no built-in flash memory.
   * @unit MB
   */
  flashCapacity: number
  /** Some devices have PSRAM in addition to their main memory (SRAM) @unit MB */
  psramCapacity: number
  /**
   * `MASS_PRODUCTION` - Most devices have this status. They are available to buy and are promoted by Espressif
   * `NOT_RECOMMENDED_FOR_NEW_DESIGNS` - Device is no longer recommended to use in future projects (i.e. deprecated)
   */
  productionStatus: ProductionStatus
  /** Total number of provided GPIO pins, including pins for strapping or flash communication */
  gpioPins: number
  /** Operating temperature of the device @unit ℃ */
  operatingTemp: Range<number>
  /** Voltage that can be supplied to the device, also the voltage it operates at @unit V */
  voltage: Range<number>
  /** @unknown */
  sizeType: string
  /** The date the product was released on */
  releaseDate?: Date
  /** The name of this specific device, e.g. ESP32-WROVER-E-N4R2 or ESP32-S3R8V */
  partName: string
  /** When buying from Espressif, the standard/minium number of products that are included in a single package */
  standardPackageQuantity: number
  /** When buying from Espressif, the minimum number of products that can be ordered at once */
  minimumOrderQuantity: number
  /** The type of antenna, if part of the product */
  antenna: string | null
  /** The series ordinal of the device in the product selector, when no sort dimension is active */
  defaultIndex: number
  /** @unknown Something to do with pre-flashing availability? */
  preFirmware: boolean | null
  /** @unknown Something to do with supported versions of esp-idf? */
  idfSupports: string | null
  /** `true` if the device has built-in Thread and Zigbee protocol support */
  threadZigbeeSupport: boolean | null
}

export interface Dimensions {
  width: number
  height: number
  depth?: number
}

export interface WiFiAbility {
  /** Whether Wi-Fi 4 (which includes back-compat) or Wi-Fi 6 is used */
  protocolVersion: 4 | 6
  /** The supported Wi-Fi frequency band @unit GHz */
  frequency: 2.4 | 5 | 6

  /** The supported data rate, in Mbps */
  maxDataRate: number
  /** Supported radio band widths, i.e. H20 (20 Mhz) or H40 (40 Mhz) */
  bandWidths: number[]
}

export interface BluetoothAbility {
  technologyType: BluetoothType
  version?: string
}

export type BluetoothType = "BLUETOOTH_CLASSIC" | "BLUETOOTH_LOW_ENERGY"
type ProductType = "SYSTEM_ON_CHIP" | "MODULE"
type ProductionStatus =
  | "MASS_PRODUCTION"
  | "NOT_RECOMMENDED_FOR_NEW_DESIGNS"
  | "SAMPLE"

export type Range<T> = {
  min: T
  max: T
}

export type PropertyTransformer<
  TInput extends keyof RawProduct,
  TOutput extends keyof ProductSpecs
> =
  | {
      /** The name of the key ot be used in the output object */
      newKey: TOutput | null
      /** Provide a function that parses the raw value and returns the new value. Can be left out if no preprocessing is required. */
      transform?(
        rawValue: RawProduct[TInput] | null,
        /** The ProductSpecs object being built, which can be directly manipulated if that's required */
        specs: any
      ): ProductSpecs[TOutput]
    }
  | {
      /** Set to `null` to state that this value won't be added to the new object */
      newKey: null
      /** Optionally, provide a function that mutates `specs` as part of its side-effects */
      transform?(
        rawValue: RawProduct[TInput] | null,
        /** The ProductSpecs object being built, which can be directly manipulated if that's required */
        specs: any
      ): void
    }

export interface ProductDescription {
  describe: string
  picture: string
  name: string
  status: string
  diagram: string
  doc: FileReference[]
  cert: FileReference[]
  devkits: DevKitData[]
}

export interface FileReference {
  name: string
  link: string
}

export interface DevKitData {
  id: number
  name: string
  link: string
  picture: string
  description: string
  feature: string
  interfaces: string
  ui: string[]
  mpn: string
  status: string
  operatingTemp: string
  flash: string
  psram: string
  dimensions: string
  antenna: string
}

export interface ProductData {
  familyName: string
  partName: string
  id: number
  specs: ProductSpecs
  description: ProductDescription
}
