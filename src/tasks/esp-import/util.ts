import is from "@sindresorhus/is"
import { FileReference, ProductData } from "./types.js"

export function stringToBoolean(string: string): string | boolean {
  const uppercaseString = string.trim().toUpperCase()
  if (uppercaseString === "YES") return true
  if (uppercaseString === "NO") return false
  if (uppercaseString === "AVAILABLE") return true
  return string
}

export type Key = string | number | symbol
export type UnknownRecord<TValue = unknown> = Record<Key, TValue>

/**
 * Replaces any strings in the object that represent a nullish value* with `null`, preserving any other strings or values
 *
 * *Strings that represent a nullish value are `"-"`, `"N/A"`, and `""` (empty string)
 */
export function normaliseNullishProperties<TValue>(
  object: UnknownRecord<TValue>
) {
  const newObject: Record<Key, TValue | null> = {}

  Object.entries(object).forEach(([key, value]) => {
    const isNullish = is.string(value) && ["", "N/A", "-"].includes(value)
    newObject[key] = isNullish ? null : value
  })

  return newObject
}

/**
 * Replaces any strings in the object that represent a boolean value* with `true` or `false`, preserving any other strings or values
 *
 * *Strings that represent a boolean value are `"YES"` and `"NO"` (case-insensitive)
 */
export function normaliseBooleanProperties<TValue>(
  object: UnknownRecord<TValue>
) {
  const newObject: Record<Key, TValue | boolean> = {}

  Object.entries(object).forEach(([key, value]) => {
    if (is.string(value)) {
      // @ts-ignore This is safe because TValue must include strings by this point
      newObject[key] = stringToBoolean(value)
    }

    newObject[key] = value
  })

  return newObject
}

/**
 * Goes though the values in an object, and if any of them are a string with special meaning, it is replaced with the native type.
 * E.g. `"N/A"` becomes `null`, and `"YES"` becomes `true`.
 * Any non-special strings are preserved.
 */
export function normaliseSpecialStringProperties<T>(rawData: UnknownRecord<T>) {
  let normalisedData = rawData
  normaliseNullishProperties(normalisedData)
  normaliseBooleanProperties(normalisedData)
  return normalisedData
}

/**
 * Finds a version number (e.g v5.1) in the provided string
 * @param string The string to search through
 * @returns The first version number found, without the `v` prefix, or `null` if nothing was found
 */
export function findVersionNumber(string: string) {
  const match = string.match(/v(\d(?:.\d)+)/)?.[1]
  return match
}

export function stringToNumber(string: string) {
  const number = parseFloat(string)
  if (Number.isNaN(number))
    throw new SyntaxError(`Couldn't parse ${number} as a number!`)
  return number
}

export function datasheetURL(product: ProductData): FileReference | null {
  const documents = product.description.doc
  const matchingFiles = documents.filter(({ name }) => {
    return /datasheet/i.test(name)
  })

  // if (matchingFiles.length === 0 && documents.length !== 0)
  //   console.warn(`Couldn't find any datasheets for ${product.partName}`)
  const haveDifferentURLs = allPropertiesIdentical(matchingFiles, "link")
  if (matchingFiles.length > 1 && haveDifferentURLs) {
    throw new Error(`Found multiple datasheet files for ${product.partName}`)
  }

  return matchingFiles[0]
}

export function allPropertiesIdentical<TObj, TKey extends keyof TObj>(
  objects: TObj[],
  key: TKey
): boolean {
  if (objects.length === 0) return true

  const firstValue = objects[0][key]

  for (const prop in objects) {
    if (objects[prop] !== firstValue) return false
  }

  return true
}
