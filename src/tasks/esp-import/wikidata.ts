/** Shorthands for Wikidata item IDs */
export const Q = {
  ESP32_FAMILY: "Q27921668",
  MICROCONTROLLER: "Q165678",
  SYSTEM_ON_CHIP: "Q610398",
  MODEL_SERIES: "Q811701",
  MODEL: "Q10929058",
} as const
