import is from "@sindresorhus/is"
import rawProducts from "./products.json" assert { type: "json" }
import {
  BluetoothType,
  Dimensions,
  ProductData,
  ProductDescription,
  ProductSpecs,
  PropertyTransformer,
  Range,
  RawProduct,
  WiFiAbility,
} from "./types.js"
import {
  datasheetURL,
  findVersionNumber,
  normaliseNullishProperties,
  stringToBoolean,
  stringToNumber,
} from "./util.js"
import { fetchBuilder, FileSystemCache } from "node-fetch-cache"

// @ts-ignore The types package doesn't appear to be properly declaring the default export, so we specify it manually
// const fetch: typeof fetchImport.default = fetchImport
const fetch = fetchBuilder.withCache(
  new FileSystemCache({
    cacheDirectory: ".cache",
  })
)

// This function ensures that type params are inferred properly
function pt<
  TInputKey extends keyof RawProduct,
  TOutputKey extends keyof ProductSpecs
>(transformerInfo: PropertyTransformer<TInputKey, TOutputKey>) {
  return transformerInfo
}

type ProductPropertyTransformers = {
  [Property in keyof RawProduct]: PropertyTransformer<
    Property,
    keyof ProductSpecs
  >
}

class Parser {
  rawData

  constructor(rawData: RawProduct[]) {
    this.rawData = rawData
  }

  parseBluetoothType(statement: string): BluetoothType {
    if (statement.match("BR/EDR")) return "BLUETOOTH_CLASSIC"
    if (statement.match("Bluetooth LE")) return "BLUETOOTH_LOW_ENERGY"
    throw new SyntaxError(`Failed to decode bluetooth statement: ${statement}`)
  }

  findSocketAndDimensions(fullString: string): {
    socketName: string | undefined
    dimensions: string
  } {
    const socketAndDimensions = /(?:(\w+) *\()?\(?([\d\.]+(?:.[\d\.]+)+)\)?/
    const match = fullString.match(socketAndDimensions)
    if (!match)
      throw new SyntaxError(`Failed to decode dimensions: ${fullString}`)

    const socketName = match[1]
    const dimensions = match[2]
    return {
      socketName,
      dimensions,
    }
  }

  toDimensions(dimensionsString: string): Dimensions {
    // Separates by the `*`,`x`, or `×` characters
    const separators = /\u00d7|\*|x/i
    const dimensions = dimensionsString.split(separators).map(stringToNumber)

    const width = dimensions.at(0)
    const height = dimensions.at(1)
    const depth = dimensions.at(2)
    if (!width || !height)
      throw new SyntaxError(
        `Couldn't extract width/height: ${dimensionsString}`
      )

    return {
      height,
      width,
      depth,
    }
  }

  toRange(rangeString: string): Range<number> {
    const separator = "~"
    const bounds = rangeString.split(separator).map(stringToNumber)

    // Ensure that index 0 and index -1 exist, and aren't the same thing
    if (bounds.length > 2)
      throw new SyntaxError(
        `Couldn't extract upper/lower bounds: ${rangeString}`
      )

    const lowerBound = bounds.at(0)!
    const upperBound = bounds.at(-1)!

    return {
      min: lowerBound,
      max: upperBound,
    }
  }

  parseWifiAbility(abilityString: string): WiFiAbility {
    const statementSeparator = ";"
    const statements = abilityString.split(statementSeparator)
    let maxSpeed: number | null = null,
      bandWidths: number[] | null = null,
      frequency: 2.4 | 5 | 6 | null = null,
      protocolVersion: 4 | 6 | null = null
    statements.forEach((statement) => {
      // PARSE PROTOCOL VERSION
      if (statement.match("IEEE 802.11 b/g/n")) protocolVersion = 4
      if (statement.match("IEEE 802.11 ax")) protocolVersion = 6

      // PARSE FREQUENCY BAND
      const frequencyMatch = statement.match(/([\d\.]+) GHz/)
      if (frequencyMatch) {
        const frequencyValue = stringToNumber(frequencyMatch[1])
        if (![2.4, 5, 6].includes(frequencyValue))
          throw new TypeError(
            `${frequencyValue} GHz is not a valid Wi-Fi frequency band!`
          )

        // @ts-ignore We've asserted that the frequency is valid in the above if statement
        frequency = frequencyValue
      }

      // PARSE MAXIMUM CONNECTION SPEED
      const maxSpeedMatch = statement.match(/([\d\.]+) Mbps/i)
      if (maxSpeedMatch) {
        maxSpeed = stringToNumber(maxSpeedMatch[1])
      }

      // PARSE RADIO BAND WIDTH
      const bandWidthMatch = statement.match(/HT([\d\/]+)/)
      if (bandWidthMatch) {
        // Turns "HT20/40" into [20, 40]
        bandWidths = bandWidthMatch[1].split("/").map(stringToNumber)
      }
    })

    if (!maxSpeed || !bandWidths || !frequency || !protocolVersion) {
      throw new SyntaxError(`Couldn't get all required Wi-Fi characteristics`)
    }

    return {
      bandWidths,
      frequency,
      maxDataRate: maxSpeed,
      protocolVersion,
    }
  }

  propertyTransformers: ProductPropertyTransformers = {
    antenna: pt({
      newKey: "antenna",
    }),
    _XID: pt({
      newKey: null,
    }),
    bluetooth: pt({
      newKey: "bluetooth",
      transform: (rawValue) => {
        const statements = rawValue?.split("+").map((s) => s.trim()) || []
        return statements.map((statement) => {
          const technologyType = this.parseBluetoothType(statement)
          const version = findVersionNumber(statement)
          return {
            technologyType,
            version,
          }
        })
      },
    }),
    dimensions: pt({
      newKey: "dimensions",
      transform: (rawValue, specs) => {
        if (!rawValue) throw new TypeError("Dimensions haven't been provided!")
        const { dimensions, socketName } =
          this.findSocketAndDimensions(rawValue)
        specs.socketName = socketName
        return this.toDimensions(dimensions)
      },
    }),
    flash: pt({
      newKey: "flashCapacity",
    }),
    freq: pt({
      newKey: "clockFrequency",
    }),
    gpio: pt({
      newKey: "gpioPins",
    }),
    id: pt({
      newKey: "id",
    }),
    idfSupports: pt({
      newKey: "idfSupports",
    }),
    moq: pt({
      newKey: "minimumOrderQuantity",
      transform: stringToNumber,
    }),
    mpn: pt({
      newKey: "partName",
    }),
    name: pt({
      newKey: null,
    }),
    nameNew: pt({
      newKey: "familyName",
    }),
    operatingTemp: pt({
      newKey: "operatingTemp",
      transform: (rawValue) => {
        if (!rawValue)
          throw new TypeError("Operating temperature hasn't been provided!")
        return this.toRange(rawValue)
      },
    }),
    pins: pt({
      newKey: "pins",
    }),
    position: pt({
      newKey: "defaultIndex",
    }),
    preFirmware: pt({
      newKey: "preFirmware",
    }),
    psram: pt({
      newKey: "psramCapacity",
    }),
    releaseTime: pt({
      newKey: "releaseDate",
      transform(rawValue) {
        return rawValue ? new Date(rawValue) : undefined
      },
    }),
    rom: pt({
      newKey: "romCapacity",
    }),
    sizeType: pt({
      newKey: "sizeType",
    }),
    spq: pt({
      newKey: "standardPackageQuantity",
      transform: stringToNumber,
    }),
    sram: pt({
      newKey: "sramCapacity",
    }),
    status: pt({
      newKey: "productionStatus",
      transform(rawValue) {
        if (!rawValue) throw new TypeError("Status hasn't been provided!")
        if (rawValue.match("Mass Production")) return "MASS_PRODUCTION"
        if (rawValue.match("NRND")) return "NOT_RECOMMENDED_FOR_NEW_DESIGNS"
        if (rawValue.match("Sample")) return "SAMPLE"
        throw new SyntaxError(`Couldn't decode status: ${rawValue}`)
      },
    }),
    threadZigbee: pt({
      newKey: "threadZigbeeSupport",
      transform(rawValue) {
        if (!rawValue) return null
        const parsed = stringToBoolean(rawValue)
        if (!is.boolean(parsed))
          throw SyntaxError(`Couldn't convert to boolean: ${rawValue}`)

        return parsed
      },
    }),
    type: pt({
      newKey: "type",
      transform(rawValue) {
        if (!rawValue) throw new TypeError()
        if (rawValue.match("SoC")) return "SYSTEM_ON_CHIP"
        if (rawValue.match("Module")) return "MODULE"
        throw new SyntaxError(`Couldn't decode product type: ${rawValue}`)
      },
    }),
    voltageRange: pt({
      newKey: "voltage",
      transform: (rawValue) => {
        if (!rawValue) throw new TypeError("Voltage hasn't been provided!")
        return this.toRange(rawValue)
      },
    }),
    wifi: pt({
      newKey: "wifi",
      transform: (rawValue) => {
        if (!rawValue) return []
        return [this.parseWifiAbility(rawValue)]
      },
    }),
    wifi6: pt({
      newKey: null,
      transform: (rawValue, specs) => {
        if (!rawValue) return
        specs.wifi.push(this.parseWifiAbility(rawValue))
      },
    }),
  }

  async fetchProductDescription(id: number): Promise<ProductDescription> {
    const url = new URL(
      "https://products.espressif.com/api/user/product/description"
    )
    url.searchParams.set("language", "en")
    url.searchParams.set("id", id.toString())

    const responseData = await fetch(url).then((res) => res.json())
    return responseData as ProductDescription
  }

  getSpecs(): ProductSpecs[] {
    const productsSpecs: ProductSpecs[] = []

    rawProducts.forEach((rawData) => {
      const normalisedData = normaliseNullishProperties(rawData)
      const productSpecs: Partial<ProductSpecs> = {}

      Object.entries(this.propertyTransformers).forEach(
        ([oldKey, transformer]) => {
          const oldValue = normalisedData[oldKey]
          const newValue = transformer.transform
            ? // @ts-ignore We know that `oldValue` is the correct type each time
              transformer.transform(oldValue, productSpecs)
            : oldValue

          if (!transformer.newKey) return
          // @ts-ignore Once again, we know that `newValue` will be the correct type here
          productSpecs[transformer.newKey] = newValue
        }
      )

      productsSpecs.push(productSpecs as ProductSpecs)
    })

    return productsSpecs
  }

  async getFullProductData(): Promise<ProductData[]> {
    const products = this.getSpecs()
    const fullData: ProductData[] = await Promise.all(
      products.map(async (specs) => {
        const { id, familyName, partName } = specs
        const description = await this.fetchProductDescription(id)
        const data: ProductData = {
          id,
          description,
          familyName,
          partName,
          specs,
        }
        return data
      })
    )

    return fullData
  }

  async getDatasheets() {
    const products = await this.getFullProductData()
    const datasheets: { family: string; title: string; url: string }[] = []
    products.forEach((product) => {
      /** If we've already listed a datasheet for this family, it'll be copied to this variable. */
      const matchingDatasheet = datasheets.find(
        ({ family }) => family === product.familyName
      )
      /** The datasheet URL of the current product */
      const currentDatasheet = datasheetURL(product)

      // Some products don't have datasheets. We'll ignore them.
      if (!currentDatasheet) return

      if (
        matchingDatasheet &&
        matchingDatasheet.url !== currentDatasheet.link
      ) {
        // This means that we've already listed a datasheet for this family, but it has a different URL than the current one
        throw new Error(`Mismatched datasheet URLs for ${product.familyName}`)
      }

      // We don't need to list the same datasheet twice
      if (matchingDatasheet) return

      datasheets.push({
        family: product.familyName,
        title: currentDatasheet.name,
        url: currentDatasheet.link,
      })
    })

    return datasheets
  }
}

const parser = new Parser(rawProducts)
const data = await parser.getFullProductData()
console.log(data)
const datasheets = await parser.getDatasheets()
console.log(datasheets)
debugger
