# ESP Import

This task is for creating and updating items for every variant of the ESP32 microcontroller, with data sourced from Espressif's official ESP Product Selector.

## Explanation of files

- `products.json` contains the data for all the products (i.e. system-on-chip models and modules) that feature on the Product Selector (sourced from the JS console)
- `parse-product-data.ts` takes the data in `products.json` and converts it into a more useful format
- `util.ts` contains general utility functions
- `types.ts` contains general types, mainly for the data structures produced by `parse-product-data.ts`
- `wikidata.ts` contains utilities for Wikidata, such as providing named item IDs
