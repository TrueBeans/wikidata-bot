# BEANS Bot

A tool for making automated edits to Wikidata using a [bot account](https://www.wikidata.org/wiki/Wikidata:Bots). It currently powers [User:BEANS Bot](https://www.wikidata.org/wiki/User:BEANS_Bot) in its mission to replace deprecated uses of [the "of" qualifier](https://www.wikidata.org/wiki/Property:P642).

For more information about the purpose of this script, read the Request for Permission at <https://www.wikidata.org/wiki/Wikidata:Requests_for_permissions/Bot/BEANS_Bot>.

This project uses Node.js, Typescript and Yarn v3. To query and edit wikidata, it uses `wikibase-sdk` and `wikibase-edit`. It's licensed under the MIT license. Check the [package.json](package.json) for more metadata.

## Usage

### Requirements

* Your system must have [Node.js](https://nodejs.org/) v18 or greater and [Yarn](https://yarnpkg.com/getting-started/install) v3 installed and accessible through the terminal
* You need a [bot account](https://www.wikidata.org/wiki/Wikidata:Bots) on Wikidata. Before you run the script, you'll need to go through the bot approval process to gain the bot flag
* You'll have to [generate a "bot password"](https://www.mediawiki.org/wiki/Special:BotPasswords) for your bot account to authenticate, see [the info about getting a password](#wikidata_password) below

### Instructions

1. Clone this repository locally and navigate to the folder
2. Install the required dependencies by running `yarn`
3. Compile the source code by running `yarn build`
4. To specify your configuration, check the [instructions for creating a `.env` file](#environment-variables)
5. Once you've set up the `.env` file, run `yarn start` to execute the script

## Environment variables

Configuration for this project is done using runtime environment variables. You should create a `.env` file at the root of this project to specify them

### Minimal example

This configuration will use the **BEANS Bot** account to make 10 edits to the production instance of Wikidata, using the provided edit summary. See the [WIKIDATA_PASSWORD](#wikidata_password) section for how to obtain credentials.

```bash
WIKIDATA_USERNAME="BEANS Bot" # Set this to your bot account's username
WIKIDATA_PASSWORD="Property_bot@27dii26u8xzf7871dpy" # Use the bot password that you've created
WIKIDATA_INSTANCE="https://www.wikidata.org"
SPARQL_ENDPOINT="https://query.wikidata.org/sparql"
WIKIDATA_MAX_EDITS=10
WIKIDATA_EDIT_SUMMARY='Replace the [[Property:P642|"of" qualifier]] with [[Property:P2700|"protocol"]] for specifying port numbers'
```

### `WIKIDATA_USERNAME`

This must be the name of the account that edits will be made from. This is not usually the same as the "bot name" used to generate bot passwords (see [the section on passwords](#wikidata_password)).

You don't have to use a bot account to run this script, but [Wikidata's policy](https://www.wikidata.org/wiki/Wikidata:Bots) requires that you use an approved bot account when making automated edits on their site. Be sure to [enable the bot flag](#wikidata_bot) if editing Wikidata with this script.

### `WIKIDATA_PASSWORD`

This is a string used to authenticate with the Wikidata API. This script uses a feature called "bot passwords" to create a special, single-purpose password for wiki accounts ([read more about bot passwords](https://www.mediawiki.org/wiki/Manual:Bot_passwords)). To generate one, log in to mediawiki.org with the account specified in `WIKIDATA_USERNAME`. Then, go to [Special:BotPasswords](https://www.mediawiki.org/wiki/Special:BotPasswords) and create a new bot password with the **edit existing pages** permission. Make note of the password in the "old" format, e.g. `Property_bot@27dii2soaf6u8xzf7mgfwx8wmfk71dpy`, to use as the value for `WIKIDATA_PASSWORD`.

### `WIKIDATA_INSTANCE`

This is a URL that points to the wiki that will be edited. The script is designed to perform chores on wikidata.org, so if you're editing another wiki you'll also need to edit the property IDs in the source code.

The recommended value is `https://www.wikidata.org`, and you must include the `www` subdomain and ensure that `https` is used.

### `SPARQL_ENDPOINT`

This is a URL, similar to `WIKIDATA_INSTANCE`. It specifies how to access the SPARQL API, which is used to find statements that need editing. If you're editing an alternative wiki to Wikidata, ensure that you specify a SPARQL endpoint for your wiki (if it doesn't support SPARQL, this script won't work). Otherwise, you should use `https://query.wikidata.org/sparql`, the Wikidata query endpoint.

### `WIKIDATA_MAX_EDITS`

This specifies the maximum number of edits that the script will make (one edit is made for each updated statement). The total number of edits is also limited by how many results the query returns (i.e. the number of statements that need fixing).

To check that the bot is working properly, you can set this to `1` to make a single edit. Once you're ready to fix everything, set it to a high number like `1000`.

### `WIKIDATA_EDIT_SUMMARY`

This specifies the [edit summary](https://www.wikidata.org/wiki/Help:Edit_summary) that will be used for each edit. It should tersely describe the change that has been made.

### `WIKIDATA_BOT`

This optional variable can be set to `true` to attach the "bot" flag to edits made by the script. This can be used to identify automated edits when browsing Recent Changes, so it's recommended to enable this. If you're editing Wikidata, [their policy](https://www.wikidata.org/wiki/Wikidata:Bots#All_bots) requires the bot flag to be enabled.

Note that for the bot flag to work, your bot account must be [approved](https://www.wikidata.org/wiki/Wikidata:Bots#Approval_process).

### `DRY_RUN`

This optional variable can be useful for testing the scripts query system. When set to `true`, the script will print all the statements that it intends to edit, without actually making any edits.

### Known issues

* I haven't implemented any mitigations against network errors, and the script will simply exit if an API request fails. Be prepared to manually restart it.
